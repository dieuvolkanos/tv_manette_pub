#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <IRremoteESP8266.h>
#include <IRsend.h>
#include <WiFiClient.h>
#include <PubSubClient.h>

#define TV_VOLUME_UP    0x2FD58A7
#define TV_VOLUME_DOWN  0x2FD7887
#define TV_POWER_ON     0x2FD48B7
#define TV_POWER_OFF    0x2FD48B7
#define TV_SLEEP        0x2FDA857 
#define TV_MUTE         0x2FD08F7
#define TV_INPUT        0x2FDF00F
#define TV_FLECHE_HAUT    0x2FD41BE 
#define TV_FLECHE_BAS     0x2FDC13E 
#define TV_FLECHE_GAUCHE  0x2FDB847 
#define TV_FLECHE_DROITE  0x2FD9867 
#define TV_MENU           0x2FD01FE 
#define TV_EXIT           0x2FD1AE5 
#define TV_ENTER          0x2FD916E 


const char* ssid = "SSID_WIFI";
const char* password = "Mot_de_Passe_WIFI";

const char* mqttServer = "10.0.0.235";
const int mqttPort = 1883;
const char* mqttUser = "MQTT_USER";
const char* mqttPassword = "MOT_DE_PASSE";

WiFiClient espClient;
PubSubClient client(espClient);

IRsend irsend(4);  // An IR LED is controlled by GPIO pin 4 (D2)

void setup_wifi(){
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}


void setup(void) {
  Serial.begin(115200);
  setup_wifi();  
  irsend.begin();
  
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
}

void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();

  // Feel free to add more if statements to control more GPIOs with MQTT

  // If a message is received on the topic esp32/output, you check if the message is either "on" or "off". 
  // Changes the output state according to the message
  if (String(topic) == "tvvolumeup") {
    Serial.print("VolumeUP=");
    if(messageTemp == "toggle"){
      Serial.println("toggle");
      irsend.sendNEC(TV_VOLUME_UP, 32);
    }
  }
  if (String(topic) == "tvvolumedown") {
    Serial.print("VolumeDown=");
    if(messageTemp == "toggle"){
      Serial.println("toggle");
      irsend.sendNEC(TV_VOLUME_DOWN, 32);
    }
  }
  if (String(topic) == "tvsleep") {
    Serial.print("Sleep=");
    if(messageTemp == "toggle"){
      Serial.println("toggle");
      irsend.sendNEC(TV_SLEEP, 32);
    }
  }
  if (String(topic) == "tv") {
    Serial.print("TV=");
    if(messageTemp == "ON"){
      Serial.println("AllumeTV");
      irsend.sendNEC(TV_POWER_ON, 32);
    }
    else{
      Serial.println("EteindreTV");
      irsend.sendNEC(TV_POWER_OFF, 32);
    }
    
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("TV_ESP3266",mqttUser, mqttPassword)) {
      Serial.println("connected");
      // Subscribe
      client.subscribe("tvvolumeup");
      client.subscribe("tvvolumedown");
      client.subscribe("tv");
      client.subscribe("tvsleep");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


void loop(void) {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();  
}
